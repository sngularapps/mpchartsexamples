package com.trecone.charts;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.charts.RadarChart;
import com.trecone.charts.charts.TrecoBarChart;
import com.trecone.charts.charts.TrecoLineChart;
import com.trecone.charts.charts.TrecoPieChart;
import com.trecone.charts.charts.TrecoRadarChart;

public class MainActivity extends AppCompatActivity {

    BarChart barChart;
    PieChart pieChart;
    LineChart lineChart;
    RadarChart radarChart;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        loadViews();
        loadListeners();

    }

    private void loadListeners() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void loadViews() {
        barChart = (BarChart) findViewById(R.id.barChart);
        pieChart = (PieChart) findViewById(R.id.pieChart);
        lineChart = (LineChart) findViewById(R.id.lineChart);
        radarChart = (RadarChart) findViewById(R.id.radarChart);
        fab = (FloatingActionButton) findViewById(R.id.fab);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        String message = "";
        switch (id){
            case R.id.action_month:
                barChart.setVisibility(View.VISIBLE);
                pieChart.setVisibility(View.GONE);
                lineChart.setVisibility(View.GONE);
                radarChart.setVisibility(View.GONE);

                TrecoBarChart trecoBarChart = new TrecoBarChart(barChart);
                barChart = trecoBarChart.getChart();
                barChart.invalidate();
                message = "Month";
                break;
            case R.id.action_week:
                barChart.setVisibility(View.GONE);
                pieChart.setVisibility(View.VISIBLE);
                lineChart.setVisibility(View.GONE);
                radarChart.setVisibility(View.GONE);

                TrecoPieChart trecoPieChart = new TrecoPieChart(pieChart);
                pieChart = trecoPieChart.getPieChart();
                pieChart.invalidate();
                message = "Week";
                break;
            case R.id.action_day:
                barChart.setVisibility(View.GONE);
                pieChart.setVisibility(View.GONE);
                lineChart.setVisibility(View.VISIBLE);
                radarChart.setVisibility(View.GONE);

                TrecoLineChart trecoLineChart = new TrecoLineChart(lineChart);
                lineChart = trecoLineChart.getLineChart();
                lineChart.invalidate();
                message = "Day";
                break;
            case R.id.action_frequency:
                barChart.setVisibility(View.GONE);
                pieChart.setVisibility(View.GONE);
                lineChart.setVisibility(View.GONE);
                radarChart.setVisibility(View.VISIBLE);

                TrecoRadarChart trecoRadarChart = new TrecoRadarChart(radarChart);
                radarChart = trecoRadarChart.getRadarChart();
                radarChart.invalidate();
                message = "Frequency";
                break;
        }

        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();

        return super.onOptionsItemSelected(item);
    }
}
