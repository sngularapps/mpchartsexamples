package com.trecone.charts.charts;


import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.trecone.charts.R;

import java.util.ArrayList;

public class TrecoBarChart {

    BarChart chart;

    public TrecoBarChart(BarChart chart){
        ArrayList<BarEntry> valsComp1 = new ArrayList<>();
        //ArrayList<BarEntry> valsComp2 = new ArrayList<>();

        BarEntry c1e1 = new BarEntry(100.000f, 0);
        valsComp1.add(c1e1);
        BarEntry c1e2 = new BarEntry(50.000f, 1);
        valsComp1.add(c1e2);
        BarEntry c1e3 = new BarEntry(50.000f, 2);
        valsComp1.add(c1e3);


        /*BubbleEntry c2e1 = new BubbleEntry(0, 120.000f, 120.000f);
        valsComp2.add(c2e1);
        BubbleEntry c2e2 = new BubbleEntry(1, 110.000f, 110.000f);
        valsComp2.add(c2e2);
        BubbleEntry c2e3 = new BubbleEntry(2, 110.000f, 110.000f);
        valsComp2.add(c2e3);*/

        BarDataSet setComp1 = new BarDataSet(valsComp1, "");
        setComp1.setAxisDependency(YAxis.AxisDependency.LEFT);
        setComp1.setHighLightColor(R.color.colorAccent);
        setComp1.setColors(ColorTemplate.PASTEL_COLORS);
        /*BubbleDataSet setComp2 = new BubbleDataSet(valsComp2, "Junio");
        setComp2.setAxisDependency(YAxis.AxisDependency.LEFT);*/

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(setComp1);
        /*dataSets.add(setComp2);*/

        ArrayList<String> xVals = new ArrayList<>();
        xVals.add("Mayo");
        xVals.add("Junio");
        xVals.add("Julio");

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);


        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setAxisMinValue(0f);
        leftAxis.setDrawGridLines(false);


        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setAxisMinValue(0f);
        rightAxis.setDrawGridLines(false);

        BarData data = new BarData(xVals, dataSets);
        data.setValueTextSize(10f);

        Legend l = chart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setEnabled(false);
        chart.animateY(1000);
        chart.setDescription("Llamadas");
        chart.setData(data);
        this.chart = chart;
    }

    public BarChart getChart() {
        return chart;
    }
}
