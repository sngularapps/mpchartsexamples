package com.trecone.charts.charts;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

public class TrecoLineChart {
    LineChart lineChart;

    public TrecoLineChart(LineChart lineChart){

        ArrayList<Entry> yVals = new ArrayList<>();
        yVals.add(new Entry(50f,0));
        yVals.add(new Entry(30f,1));
        yVals.add(new Entry(20f,2));
        yVals.add(new Entry(60f,3));
        yVals.add(new Entry(10f,4));
        yVals.add(new Entry(80f,5));
        yVals.add(new Entry(55f,6));

        ArrayList<String> xVals = new ArrayList<>();
        xVals.add("Lunes");
        xVals.add("Martes");
        xVals.add("Miercoles");
        xVals.add("Jueves");
        xVals.add("Viernes");
        xVals.add("Sabado");
        xVals.add("Domingo");

        LineDataSet set1 = new LineDataSet(yVals, "Dias de la semana");
        set1.setDrawFilled(true);
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        LineData data = new LineData(xVals, dataSets);
        data.setValueTextSize(10f);

        lineChart.getAxisRight().setEnabled(false);
        lineChart.setData(data);
        lineChart.setDescription("Consumo diario");
        lineChart.animateXY(2500,2500);

        this.lineChart = lineChart;
    }

    public LineChart getLineChart() {
        return lineChart;
    }
}
