package com.trecone.charts.charts;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class TrecoRadarChart {
    private RadarChart radarChart;

    public TrecoRadarChart(RadarChart radarChart){

        ArrayList<Entry> yVals1 = new ArrayList<>();
        yVals1.add(new Entry(10,0));
        yVals1.add(new Entry(2,1));
        yVals1.add(new Entry(15,2));
        yVals1.add(new Entry(25,3));
        yVals1.add(new Entry(4,4));
        yVals1.add(new Entry(30,5));
        yVals1.add(new Entry(42,6));
        yVals1.add(new Entry(12,7));
        yVals1.add(new Entry(9,8));
        yVals1.add(new Entry(20,9));

        ArrayList<String> xVals = new ArrayList<>();
        xVals.add("Persona 1");
        xVals.add("Persona 2");
        xVals.add("Persona 3");
        xVals.add("Persona 4");
        xVals.add("Persona 5");
        xVals.add("Persona 6");
        xVals.add("Persona 7");
        xVals.add("Persona 8");
        xVals.add("Persona 9");
        xVals.add("Persona 10");

        RadarDataSet set1 = new RadarDataSet(yVals1, "Contactos frecuentes");
        set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
        set1.setDrawFilled(true);

        ArrayList<IRadarDataSet> sets = new ArrayList<>();
        sets.add(set1);
        RadarData data = new RadarData(xVals, sets);

        radarChart.getLegend().setEnabled(false);
        radarChart.getYAxis().setEnabled(false);

        radarChart.setData(data);
        radarChart.animateX(
                1400,
                Easing.EasingOption.EaseInOutQuad);
        radarChart.setDescription("Números frecuentes");
        this.radarChart = radarChart;
    }

    public RadarChart getRadarChart() {
        return radarChart;
    }
}
