package com.trecone.charts.charts;

import android.graphics.Color;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class TrecoPieChart {

    PieChart pieChart;

    public TrecoPieChart(PieChart pieChart) {

        ArrayList<Entry> yVals1 = new ArrayList<>();
        yVals1.add(new Entry(50.2f,0));
        yVals1.add(new Entry(100.7f,1));
        yVals1.add(new Entry(150.6f,2));
        yVals1.add(new Entry(75.1f,3));
        yVals1.add(new Entry(15.0f,4));

        ArrayList<String> xVals = new ArrayList<>();
        xVals.add("1 - 7");
        xVals.add("8 - 14");
        xVals.add("15 - 21");
        xVals.add("22 - 28");
        xVals.add("29 - 31");

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        dataSet.setSliceSpace(1f); //espacio entre trozos
        dataSet.setSelectionShift(3f); // espacio salida cuando seleciionas el trozo

        PieData data = new PieData(xVals, dataSet);
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        pieChart.setData(data);
        pieChart.setDescription("Consumo semanal (minutos)");
        pieChart.animateXY(1400, 1400);


        Legend legend = pieChart.getLegend();
        legend.setEnabled(true);
        legend.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);

        pieChart.setDrawHoleEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);
        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.setDrawSliceText(false); //quitar el valor de xVals
        this.pieChart = pieChart;
    }

    public PieChart getPieChart() {
        return pieChart;
    }
}
